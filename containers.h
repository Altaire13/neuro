#pragma once

#include <vector>
#include <exception>

using std::vector;

// TODO: Correct use of containers: [i*rows+j] versus (i, j).

// TODO: Use wherever appropriate (Neurons?)

// TODO: Abstract tensor for all dimensions?

// TODO: Do we need default values?

// TODO: Kernels?

class DataSet;

template <typename T>
struct Matrix {
    Matrix(size_t rows = 0, size_t cols = 0) :
        rows_(rows), cols_(cols),
        data(rows * cols) {}

    Matrix(size_t rows, size_t cols, const vector<T>& input) :
            rows_(rows), cols_(cols), data(input) {
        if (input.size() != rows * cols) {
            throw std::logic_error("Wrong input size in Matrix.");
        }
    }

    // TODO: Do we need move constructor?
    
    Matrix(size_t rows, size_t cols, const vector<T>&& input) :
        rows_(rows), cols_(cols), data(input) {
        if (input.size() != rows * cols) {
            throw std::logic_error("Wrong input size in Matrix.");
        }
    }

    T& at(size_t row, size_t col) {
        return data.at(row*cols_ + col);
    }

    T& at(size_t pos) {
        return data.at(pos);
    }

    T& get(size_t row, size_t col) {
        return data[row*cols_ + col];
    }

    T& operator()(size_t row, size_t col) {
        return get(row, col);
    }

    T& operator[](size_t pos) {
        return data[pos];
    }



    size_t size() {
        return data.size();
    }

    size_t rows() {
        return rows_;
    }

    size_t cols() {
        return cols_;
    }

    vector<T> exportData() {
        return data;
    }

private:
    size_t rows_;
    size_t cols_;
private:
    vector<T> data;

    friend DataSet;
};

template <typename T>
struct Tensor {
    Tensor(size_t maps = 0, size_t rows = 0, size_t cols = 0) :
        maps_(maps), rows_(rows), cols_(cols),
        data(maps * rows * cols) {}

    Tensor(size_t maps, size_t rows, size_t cols, vector<T>& input) :
            maps_(maps), rows_(rows), cols_(cols), data(input) {
        if (input.size() != maps * rows * cols) {
            throw std::logic_error("Wrong input size in Tensor.");
        }
    }

    // TODO: is it right?

    Tensor(Matrix<T> matrix) : maps_(1), 
        rows_(matrix.rows()), cols_(matrix.cols()), data(matrix.exportData()) {
    }

    // TODO: is it right?

    Matrix<T> getMap(size_t map) {
        //return vector<T>(data.begin() + map * map_size(),
        //    data.begin() + (map + 1) * map_size());

        return Matrix<T>(rows_, cols_,
            vector<T>(data.begin() + map * map_size(),
                data.begin() + (map + 1) * map_size()));
    }


    T& at(size_t map, size_t row, size_t col) {
        return data.at(map*rows_*cols_ + row*cols_ + col);
    }

    T& at(size_t pos) {
        return data.at(pos);
    }

    T& get(size_t map, size_t row, size_t col) {
        return data[map*rows_*cols_ + row*cols_ + col];
    }

    T& operator()(size_t map, size_t row, size_t col) {
        return get(map, row, col);
    }

    T& operator[](size_t pos) {
        return data[pos];
    }



    size_t size() {
        return data.size();
    }

    size_t maps() {
        return maps_;
    }

    size_t rows() {
        return rows_;
    }

    size_t cols() {
        return cols_;
    }

    size_t map_size() {
        return rows_ * cols_;
    }

    vector<T> exportData() {
        return data;
    }

private:
    size_t maps_;
    size_t rows_;
    size_t cols_;
private:
    vector<T> data;
};


//typedef std::vector<uint8_t> Image;
typedef Matrix<uint8_t> Image;

//typedef vector<double> FloatImage;
typedef Matrix<double> FloatImage;

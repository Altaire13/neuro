#include <iostream>
#include <vector>
#include <string>

#include "mnist_loader.h"
#include "FullLayer.h"
#include "ConvLayer.h"
#include "PoolLayer.h"
#include "Net.h"

#include "bitmap.h"

using std::cout;
using std::endl;

using std::vector;
using std::string;
using std::to_string;


void saveGray(const char* filename, Image& image) {
    // TODO: Buf as Matrix?
    uint32_t* buf = new uint32_t[image.rows() * image.cols()];
    for (size_t i = 0; i < image.rows(); i++) {
        for (size_t j = 0; j < image.cols(); j++) {
            uint8_t t = image(i, j);
            buf[(image.rows()-i-1)*image.cols() + j] = (t << 16) | (t << 8) | t;
        }
    }

    CBitmap cbm;
    cbm.SetBits(buf, image.cols(), image.rows(), 0xFFu << 16, 0xFFu << 8, 0xFFu, 0xFFu << 24);
    cbm.Save(filename);
}

FloatImage toFloatImage(Image& image) {
    FloatImage fImage(image.rows(), image.cols());
    for (size_t i = 0; i < image.rows(); i++) {
        for (size_t j = 0; j < image.cols(); j++) {
            fImage(i, j) = 2.0 * image(i, j) / 255.0 - 1.0;
        }
    }
    return fImage;
}

Image toImage(FloatImage& floatImage) {
    Image image(floatImage.rows(), floatImage.cols());
    for (size_t i = 0; i < floatImage.rows(); i++) {
        for (size_t j = 0; j < image.cols(); j++) {
            image(i, j) = (floatImage(i, j) + 1) / 2 * 255;
        }
    }
    return image;
}

// TODO: Strange

void saveFloatImageAndPrintName(FloatImage floatImage,
        string ind1Name, size_t ind1, string ind2Name, size_t ind2) {
    string filename = "../" + ind1Name + to_string(ind1) + ind2Name + to_string(ind2) + ".bmp";
    cout << "Saving: " << filename << endl;
    Image image = toImage(floatImage);
    saveGray(filename.c_str(), image);
}

// TODO: Strange

void saveFloatImageTensorAndPrintName(Tensor<double> tensor,
        string ind1Name, size_t ind1, string ind2Name, size_t ind2) {
    FloatImage floatImage(tensor.maps() * tensor.rows(), tensor.cols(), tensor.exportData());
    saveFloatImageAndPrintName(floatImage, ind1Name, ind1, ind2Name, ind2);
}

int main(int argc, char* argv[]) {

    DataSet training("../train-images.idx3-ubyte", "../train-labels.idx1-ubyte");

    //training.pseudoShow(0);

    saveGray("../output.bmp", training.images[54]);

    typedef double InputT;
    typedef double WeightT;

    Net<InputT> net(6);
    net.layers[0] = new ConvLayer<InputT, WeightT>(5, 5, 2);
    dynamic_cast<ConvLayer<InputT, WeightT>*>(net.layers[0])->neurons[0] = {
        0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 
        0, -1, 0, 1, 0, 
        0, 0, 0, 0, 0, 
        0, 0, 0, 0, 0 };
    dynamic_cast<ConvLayer<InputT, WeightT>*>(net.layers[0])->neurons[1] = {
        0, 0, 0, 0, 0, 
        0, 0, -1, 0, 0,
        0, 0, 0, 0, 0,
        0, 0, 1, 0, 0,
        0, 0, 0, 0, 0 };
    net.layers[1] = new PoolLayer<InputT>(2, 2);
    net.layers[2] = new ConvLayer<InputT, WeightT>(5, 5, 2);
    dynamic_cast<ConvLayer<InputT, WeightT>*>(net.layers[2])->neurons[0] = {
        0, 0, 0, 0, 0,
        0, 0, 0, 0, 0,
        0, -1, 0, 1, 0,
        0, 0, 0, 0, 0,
        0, 0, 0, 0, 0 };
    dynamic_cast<ConvLayer<InputT, WeightT>*>(net.layers[2])->neurons[1] = {
        0, 0, 0, 0, 0,
        0, 0, -1, 0, 0,
        0, 0, 0, 0, 0,
        0, 0, 1, 0, 0,
        0, 0, 0, 0, 0 };
    net.layers[3] = new PoolLayer<InputT>(4, 4);
    net.layers[4] = new ConvLayer<InputT, WeightT>(2, 2, 2);
    dynamic_cast<ConvLayer<InputT, WeightT>*>(net.layers[4])->neurons[0] = {
        -1, 1,
        -1, 1 };
    dynamic_cast<ConvLayer<InputT, WeightT>*>(net.layers[4])->neurons[1] = {
        -1, -1,
        1, 1 };
    net.layers[5] = new FullLayer<InputT, WeightT>(2, 8);
    vector<FullLayer<InputT, WeightT>::Neuron>& neurons = 
        dynamic_cast<FullLayer<InputT, WeightT>*>(net.layers[5])->neurons;
    neurons[0] = {  1, 1,  1, 1, -1, -1, -1, -1 };
    neurons[1] = { -1, 1, -1, 1, -1,  1, -1,  1 };

    {
        size_t imNum = 0;
        size_t lastLayer = 0;
        FloatImage inputFloatImage = toFloatImage(training.images[imNum]);
        Tensor<double> inputImageSignal(inputFloatImage);
        Tensor<double> result = net.partial(inputImageSignal, lastLayer);
        for (size_t outInd = 0; outInd < result.maps(); outInd++) {
            saveFloatImageAndPrintName(result.getMap(outInd), "layerinit", lastLayer, "output", outInd);
        }

        saveFloatImageTensorAndPrintName(result, "layerinit", lastLayer, "concat", 0);
    }



    double error_sum = 0;
    size_t tries = 500;
    size_t ims = 100;
    size_t right;

    for (size_t k = 0; k < tries; k++) {
        error_sum = 0;
        right = 0;
        for (size_t i = 0; i < /*training.image_count*/ims; i++) {
            FloatImage fImage = toFloatImage(training.images[i]);
            Layer<InputT>::Signal input(fImage);
            auto output = net.compute(input);

            double tar1, tar2;

            bool r = false;

            switch (training.labels[i]) {
            case 0:
            case 6:
            case 8:
            case 9:
                if (output.at(0) > output.at(1)) {
                    r = true;
                    right++;
                }
                /*tar1 = 1.0;
                tar2 = 0.0;*/
                tar1 = 0.75;
                tar2 = 0.25;
                break;

            default:
                if (output.at(0) < output.at(1)) {
                    r = true;
                    right++;
                }
                /*tar1 = 0.0;
                tar2 = 1.0;*/
                tar1 = 0.25;
                tar2 = 0.75;
            }


            Layer<InputT>::Signal deltas(2, 1, 1);

            double error = 0;
            double t;
            t = tar1 - output.at(0);
            deltas.at(0) = -2 * output.at(0) * (1 - output.at(0)) * t;
            error += t * t;
            t = tar2 - output.at(1);
            deltas.at(1) = -2 * output.at(1) * (1 - output.at(1)) * t;
            error += t * t;
            error /= 2;


            if (k % 500 == 0) {
                cout << i << "\t";
                cout << "[" << output.at(0) << ", " << output.at(1) << "] \t";
                cout << "lab: " << (int)training.labels[i] << " \t";
                if (r) {
                    cout << "R ";
                } else {
                    cout << "W ";
                }
                cout << "Error: " << error << endl;
            }
            error_sum += error;


            net.backProp(deltas, 0.1);
        }

        if (k % 100 == 0) {
            cout << "Average error: (" << k << ") " << error_sum / ims;
            cout << "\tRight: " << 1.0 * right / ims << endl;
        }
    }

    {
        size_t layerNum = 0;

        ConvLayer<InputT, WeightT>* layer =
            dynamic_cast<ConvLayer<InputT, WeightT>*>(net.layers[layerNum]);

        for (size_t i = 0; i < layer->count; i++) {
            size_t height, width;
            FloatImage floatNeuronImage = layer->getNeuronImage(i, height, width);

            saveFloatImageAndPrintName(floatNeuronImage, "layer", layerNum, "neuron", i);
        }
    }

    for (size_t lastLayer = 0; lastLayer < net.layers.size(); lastLayer++) {
        size_t imNum = 0;
        FloatImage inputFloatImage = toFloatImage(training.images[imNum]);
        Tensor<double> inputImageSignal(inputFloatImage);
        Tensor<double> result = net.partial(inputImageSignal, lastLayer);
        for (size_t outInd = 0; outInd < result.maps(); outInd++) {
            saveFloatImageAndPrintName(result.getMap(outInd), "layer", lastLayer, "output", outInd);
        }

        //saveFloatImageAndPrintName(result.getData(), result.rows() * result.maps(), result.cols(), "layer", lastLayer, "concat", 0);
        saveFloatImageTensorAndPrintName(result, "layer", lastLayer, "concat", 0);
    }

    return 0;
}
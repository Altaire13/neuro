#pragma once

#include "Layer.h"

// TODO: Does it have to be template?
// TODO: Better data management in layers.

template<typename TInput>
class Net {
public:
    Net(uint32_t layers_count) : layers(layers_count) {}
    ~Net() {
        for (size_t i = 0; i < layers.size(); i++) {
            if (layers[i] != nullptr) {
                delete layers[i];
            }
        }
    }

    typename Layer<TInput>::Signal 
    compute(typename Layer<TInput>::Signal& input) {
        Layer<TInput>::Signal t = input;

        for (size_t i = 0; i < layers.size(); i++) {
            t = layers[i]->compute(t);
        }

        return t;
    }

    typename Layer<TInput>::Signal 
    backProp(typename Layer<TInput>::Signal& deltas, double rate) {
        for (size_t i = 0; i < layers.size(); i++) {
            deltas = layers[layers.size() - i - 1]->backProp(deltas, rate);
        }

        return deltas;
    }

    typename Layer<TInput>::Signal
    partial(typename Layer<TInput>::Signal& input, size_t lastLayer) {
        Layer<TInput>::Signal t = input;

        for (size_t i = 0; i < layers.size() && i <= lastLayer; i++) {
            t = layers[i]->compute(t);
        }

        return t;
    }

private:
public:
    vector<Layer<TInput>*> layers;
};


#include "mnist_loader.h"

#include <fstream>
#include <iostream>

using std::cout;
using std::endl;

using std::vector;

int32_t intCast(char * bytes) {
    char tmp[4];
    for (size_t i = 0; i < 4; i++) {
        tmp[i] = bytes[3 - i];
    }
    return *reinterpret_cast<int32_t*>(tmp);
}

DataSet::DataSet(const char * images_filename, const char * labels_filename) :
        images(
            readImages(images_filename, image_count, rows, cols, image_size)),
        labels(readLabels(labels_filename, labels_count)) {

    if (image_count != labels_count) {
        cout << "Counts don't match!" << endl;
    }
}
/*
void DataSet::pseudoShow(size_t index) {
    cout << (int)labels.at(index) << ":" << endl;

    Image& image = images.at(index);

    for (size_t i = 0; i < rows; i++) {
        for (size_t j = 0; j < cols; j++) {
            if (image[cols*i + j] > 191) {
                cout << (char)0xDB;
            } else if (image[cols*i + j] > 127) {
                cout << (char)0xB2;
            } else if (image[cols*i + j] > 63) {
                cout << (char)0xB1;
            } else if (image[cols*i + j] > 0) {
                cout << (char)0xB0;
            } else {
                cout << " ";
            }
        }
        cout << endl;
    }
    cout << endl;
}*/

std::vector<Image> DataSet::readImages(const char * image_filename,
        uint32_t & count, uint32_t & rows, uint32_t & cols, 
        uint32_t & image_size) {
    std::ifstream f(image_filename, std::ifstream::binary);

    uint32_t magic;
    char buf[16];
    f.read(buf, 16);

    magic = intCast(buf);
    count = intCast(buf + 4);
    rows = intCast(buf + 8);
    cols = intCast(buf + 12);

    image_size = rows * cols;

    vector< Image > images(count, Image(rows, cols));
    for (size_t i = 0; i < count; i++) {
        f.read(reinterpret_cast<char*>(images.at(i).data.data()), image_size);
    }

    return images;
}

std::vector<uint8_t> DataSet::readLabels(const char * labels_filename,
        uint32_t & count) {
    std::ifstream f(labels_filename, std::ifstream::binary);

    uint32_t magic;
    char buf[8];
    f.read(buf, 8);

    magic = intCast(buf);
    count = intCast(buf + 4);

    vector<uint8_t> labels(count);
    f.read(reinterpret_cast<char*>(labels.data()), count);

    return labels;
}

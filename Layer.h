#pragma once

#include <cinttypes>
#include <vector>

#include "containers.h"

using std::vector;

// TODO: Layer connection

// TODO: Better data between layers

template<typename TInput>
class Layer {
public:
    typedef vector<TInput> SignalT;
    typedef Tensor<typename TInput> Signal;

    Layer() {};
    ~Layer() {}

    virtual Signal compute(Signal& input) = 0;
    virtual Signal backProp(Signal& errors, double rate) = 0;

private:

};

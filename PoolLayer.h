#pragma once

#include "Layer.h"

#include <utility>

using std::pair;

template<typename TInput>
class PoolLayer : public Layer<TInput> {
public:
    PoolLayer(uint32_t w, uint32_t h) : width(w), height(h) {}
    ~PoolLayer() {}

    virtual Signal compute(Signal& input) {
        uint32_t out_width = input.cols() / width;
        uint32_t out_height = input.rows() / height;

        if (input.cols() % width != 0 || input.rows() % height != 0) {
            cout << "PoolLayer dimensions don't match." << endl;
        }

        Signal output(input.maps(), out_width, out_height);
        lastChoices = Tensor< pair<size_t, size_t> >(input.maps(), out_height, out_width);

        for (size_t k = 0; k < input.maps(); k++) {
            for (size_t i = 0; i < out_height; i++) {
                for (size_t j = 0; j < out_width; j++) {
                    size_t si = i * height;
                    size_t sj = j * width;

                    TInput m = input(k, si, sj);

                    size_t mi = 0;
                    size_t mj = 0;

                    for (size_t ii = 0; ii < height; ii++) {
                        for (size_t jj = 0; jj < width; jj++) {
                            TInput t = input(k, si + ii, sj + jj);

                            if (t > m) {
                                m = t;

                                mi = ii;
                                mj = jj;
                            }
                        }
                    }

                    output(k, i, j) = m;

                    lastChoices(k, i, j) = std::make_pair(mi, mj);
                }
            }
        }

        return output;
    }

    virtual Signal backProp(Signal& errors, double rate) {
        Signal output(errors.maps(), 
            height * errors.rows(), width * errors.cols());

        for (size_t k = 0; k < errors.maps(); k++) {
            for (size_t i = 0; i < errors.rows(); i++) {
                for (size_t j = 0; j < errors.cols(); j++) {
                    pair<size_t, size_t> pos = lastChoices(k, i, j);
                    output(k, i*height + pos.first, j*width + pos.second) =
                        errors(k, i, j);
                }
            }
        }

        return output;
    }

private:
public:
    uint32_t width;
    uint32_t height;

    Tensor< pair<size_t, size_t> > lastChoices;
};

#pragma once

#include "Layer.h"

#include "containers.h"

// TODO: Are Neurons better to be Matrices/Tensors?

// TODO: Abstract activation

template<typename TInput, typename TWeight>
class ConvLayer : public Layer<TInput> {
public:
    typedef vector<TWeight> Neuron;

    ConvLayer(uint32_t w, uint32_t h, uint32_t count) : width(w), height(h), 
            count(count), neurons(count, Neuron(width * height)) {};
    ~ConvLayer() {};

    virtual Signal compute(Signal& input) {
        lastInput = input;

        uint32_t out_width = input.cols() - width + 1;
        uint32_t out_height = input.rows() - height + 1;

        Signal output(input.maps() * count, out_width, out_height);
        lastSum = Signal(input.maps() * count, out_width, out_height);

        for (size_t k = 0; k < input.maps(); k++) {
            for (size_t p = 0; p < count; p++) {
                for (size_t i = 0; i < out_height; i++) {
                    for (size_t j = 0; j < out_width; j++) {
                        TInput sum = 0;

                        for (size_t ii = 0; ii < height; ii++) {
                            for (size_t jj = 0; jj < width; jj++) {
                                sum += input(k, i + ii, j + jj) * neurons[p][ii*width + jj];
                            }
                        }

                        lastSum(k * count + p, i, j) = sum;
                        output(k * count + p, i, j) = activate(sum);
                    }
                }
            }
        }

        return output;
    }

    virtual Signal backProp(Signal& errors, double rate) {
        Signal deltas(errors.maps(), errors.rows(), errors.cols());

        for (size_t k = 0; k < errors.maps(); k++) {
            for (size_t i = 0; i < errors.rows(); i++) {
                for (size_t j = 0; j < errors.cols(); j++) {
                    deltas(k, i, j) =
                        errors(k, i, j) * activateDeriviate(lastSum(k, i, j));
                }
            }
        }

        Signal output(errors.maps() / count, errors.rows() + height - 1, errors.cols() + width - 1);

        for (size_t k = 0; k < output.maps(); k++) {
            for (size_t i = 0; i < output.rows(); i++) {
                for (size_t j = 0; j < output.cols(); j++) {
                    TInput sum = 0;

                    for (size_t p = 0; p < count; p++) {
                        size_t ii = 0, jj = 0;

                        if (i >= errors.rows()) {
                            ii = i - errors.rows() + 1;
                        }

                        if (j >= errors.cols()) {
                            jj = j - errors.cols() + 1;
                        }

                        for (; ii < height && ii <= i; ii++) { //...
                            for (; jj < width && jj <= j; jj++) {
                                sum += deltas(k*count + p, i - ii, j - jj) * neurons[p][ii*width + jj];
                            }
                        }
                    }

                    output(k, i, j) = sum;
                }
            }
        }

        for (size_t p = 0; p < count; p++) {
            for (size_t ii = 0; ii < height; ii++) {
                for (size_t jj = 0; jj < width; jj++) {
                    TInput sum = 0;

                    for (size_t k = 0; k < output.maps(); k++) {
                        for (size_t i = 0; i < output.rows() - height + 1; i++) {
                            for (size_t j = 0; j < output.cols() - width + 1; j++) {
                                sum += deltas(k * count + p, i, j) * lastInput(k, i + ii, j + jj);
                            }
                        }
                    }

                    neurons[p][ii*width + jj] -= rate * sum;
                }
            }
        }

        return output;
    }

    static TInput activate(TInput sum) {
        const double a = 1;


        //return sum / (abs(sum) + 0.1);

        return 1 / (1 + exp(-2 * a * sum));

        //return 2 / (1 + exp(-2 * a * sum)) - 1;
    }

    static TInput activateDeriviate(TInput sum) {
        const double a = 1;


        //None

        double t = activate(sum);
        return 2 * a * t * (1 - t);

        //double t = activate(sum);
        //return a * (1 - t * t);
    }

    FloatImage getNeuronImage(size_t index, size_t& _height, size_t& _width) {
        Neuron cur = neurons.at(index);

        _height = height;
        _width = width;

        FloatImage image(height, width);

        for (size_t y = 0; y < height; y++) {
            for (size_t x = 0; x < width; x++) {
                cout << cur[y*width + x] << " ";
            }
            cout << endl;
        }

        for (size_t y = 0; y < height; y++) {
            for (size_t x = 0; x < width; x++) {
                cout << activate(cur[y*width + x]) << " ";
            }
            cout << endl;
        }

        for (size_t i = 0; i < height; i++) {
            for (size_t j = 0; j < width; j++) {
                image(i, j) = activate(cur[i*width + j]);
            }
        }

        return image;
    }

private:
public:
    uint32_t width;
    uint32_t height;
    uint32_t count;

    vector<Neuron> neurons;
    Signal lastInput;
    Signal lastSum;
};

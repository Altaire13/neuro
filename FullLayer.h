#pragma once
#include "Layer.h"

// TODO: Are Neurons better to be Matrices/Tensors?

// TODO: Abstract activation

template<typename TInput, typename TWeight>
class FullLayer :
    public Layer<TInput> {
public:
    typedef vector<TWeight> Neuron;

    FullLayer(size_t neuron_count, size_t input_size) : input_size(input_size),
        neurons(neuron_count, Neuron(input_size)) {};
    ~FullLayer() {};

    virtual Signal compute(Signal& input) {
        lastInput = input;

        Layer<TInput>::Signal result(neurons.size(), 1, 1);

        if (input.size() != input_size) {
            cout << "Full layer input don't match." << endl;
        }

        lastSum = SignalT(result.size());
        lastOutput = SignalT(result.size());

        for (size_t j = 0; j < neurons.size(); j++) {
            TInput sum = 0;
            for (size_t i = 0; i < input.maps(); i++) {
                for (size_t ii = 0; ii < input.map_size(); ii++) {
                    sum += neurons[j][i*input.map_size() + ii] * input(i, 0, ii);
                }
            }

            lastSum[j] = sum;
            result[j] = activate(sum);
            lastOutput[j] = result[j];
        }

        return result;
    }

    virtual Signal backProp(Signal& errors, double rate) {
        Signal output(input_size, 1, 1);

        if (errors.size() != neurons.size()) {
            cout << "Wrong errors number in Full Layer." << endl;
        }

        if (errors.cols() != 1 || errors.rows() != 1) {
            cout << "Wrong errors dimensions." << endl;
        }

        SignalT deltas(errors.size());

        for (size_t j = 0; j < neurons.size(); j++) {
            deltas[j] = errors[j] * activateDeriviate(lastSum[j]);
        }

        for (size_t i = 0; i < input_size; i++) {
            TInput sum = 0;
            for (size_t j = 0; j < neurons.size(); j++) {
                sum += deltas[j] * neurons[j][i];
            }
            output[i] = sum;
        }

        for (size_t j = 0; j < neurons.size(); j++) {
            for (size_t i = 0; i < input_size; i++) {
                neurons[j][i] -= rate * lastInput[i] * deltas[j];
            }
        }

        return output;
    }

    static TInput activate(TInput sum) {
        const double a = 1;


        //return sum / (abs(sum) + 0.1);

        return 1 / (1 + exp(-2 * a * sum));

        //return 2 / (1 + exp(-2 * a * sum)) - 1;
    }

    static TInput activateDeriviate(TInput sum) {
        const double a = 1;


        //None

        double t = activate(sum);
        return 2 * a * t * (1 - t);

        //double t = activate(sum);
        //return a * (1 - t * t);
    }

    Signal lastInput;
    SignalT lastSum;
    SignalT lastOutput;

    uint32_t input_size;
    vector<Neuron> neurons;
};


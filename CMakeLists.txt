#Quick start of cmake for Visual Studio:
#https://cognitivewaves.wordpress.com/cmake-and-visual-studio/

cmake_minimum_required(VERSION 3.4.2)

#Defines solution 
project(neuro)

#Set configuration types (from cmake FAQ)
#First line checks if it's available for selected generator
if(CMAKE_CONFIGURATION_TYPES)
    set(CMAKE_CONFIGURATION_TYPES Debug Release)
    set(CMAKE_CONFIGURATION_TYPES "${CMAKE_CONFIGURATION_TYPES}" CACHE STRING
        "Reset the configurations to what we need"
        FORCE)
endif()

if(MSVC)
  # Force to always compile with W4
  if(CMAKE_CXX_FLAGS MATCHES "/W[0-4]")
    string(REGEX REPLACE "/W[0-4]" "/W4" CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS}")
  else()
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /W4")
  endif()
elseif(CMAKE_COMPILER_IS_GNUCC OR CMAKE_COMPILER_IS_GNUCXX)
  # Update if necessary
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wno-long-long -pedantic")
endif()

#Set source list for project
set(NEURO_SOURCE 
        "mnist_loader.h"
        "Net.h"
        "Layer.h"
        "FullLayer.h"
        "ConvLayer.h"
        "PoolLayer.h"
        "containers.h"
        
        "mnist_loader.cpp"
        "main.cpp")

#Set build result
add_executable(neuro ${NEURO_SOURCE})
#target_link_libraries(neuro)

#pragma once

#include <vector>
#include <cstdint>

#include "containers.h"

using std::vector;


int32_t intCast(char * bytes);

//TODO: Abstract idx file loader

class DataSet {
public:
    DataSet(const char* image_filename, const char* labels_filename);
    ~DataSet() {};

    void pseudoShow(size_t index);

private:
    static std::vector<Image> readImages(const char *image_filename,
        uint32_t & count, uint32_t & rows, uint32_t & cols, 
        uint32_t & image_size);

    static std::vector<uint8_t> readLabels(const char *labels_filename,
        uint32_t & count);

public:
    uint32_t rows;
    uint32_t cols; 
    uint32_t image_count;
    uint32_t labels_count;

    size_t image_size;

    vector<Image> images;
    vector<uint8_t> labels;
};
